#!/bin/bash

action=$1
shift

[ "$action" = "usage" ] && {
  echo "  ls only todo tasks which have a priority set to them from (A)...(Z | LETTER) :"
  curcmd=`basename $0`
  echo "    $curcmd [LETTER] \"THING I NEED TO DO +project @context\""
  echo ""
  exit
}

REGEX=""
TOTAL=0

run_lsp() {
  TODO_FILE_DIR=`dirname $TODO_FILE`
  ACCESS_FILE="$HOME/.todo/access.yaml"
  END_TXT=""

  if [ -f "$ACCESS_FILE" ]; then
    SHOW_FILES="`ls $TODO_FILE_DIR/*todo.txt | ~/.todo.actions.d/lib/access-filter.py | sort -r`"
    # for testing
    # ls $TODO_FILE_DIR/*todo.txt | ~/.todo.actions.d/lib/access-filter.py
    # exit
  else
    SHOW_FILES="`ls $TODO_FILE_DIR/*todo.txt | sort -r`"
  fi

  for f in $SHOW_FILES; do
    # set done_fn
    base_fn="$(basename $f)"
    base_name="$(echo $base_fn | cut -d'.' -f1)"
    done_fn=done.txt
    [ ! "$base_fn" == "todo.txt" ] && \
      done_fn="${base_name}.done.txt"

    # get done tasks
    done_tasks=""
    # future_tasks=""
    for fn in $TODO_FILE_DIR/{$done_fn,$base_fn}; do
      [[ ! -f $fn ]] && continue
      done_tasks+="$(cat $fn | ag '^x '$(gdate --date today '+%Y-%m-%d'))"
      done_tasks+="$(cat $fn | ag '^x '$(gdate --date yesterday '+%Y-%m-%d'))"
      # for dt in tomorrow; do
      #   future_tasks+="$(cat $TODO_FILE_DIR/$fn | ag 't:'$(date --date $dt '+%Y-%m-%d'))"
      # done
    done

    # get tasks and count them
    text=`$TODO_SH command lf $f "$REGEX"`
    num=`echo $text \
      | awk ' \
          match($0,/of ([0-9]+)/) { \
            print substr($0,RSTART+3,RLENGTH-3) \
          }'`
    TOTAL=$(($TOTAL + $num))

    # only print tasks if there are more than 1
    if [[ ! "$text" == *" 0 of "* ]]; then
      while read -r line; do
        printf "\e[2;37m%s\e[0m\n" "$line"
      done <<< "$done_tasks"
      echo "$text"
      while read -r line; do
        printf "\e[2;37m%s\e[0m\n" "$line"
      done <<< "$future_tasks"
    else
      END_TXT+="`echo $text | tail -n1`\n"
    fi
  done
  echo -e "$END_TXT"
  echo "ALL: $TOTAL total tasks"
}


[ "$action" = "lsp" ] && {

  # only get pri up until letter given
  arg=$1; shift
  ALP="$(echo {A..D})"
  for a in $ALP; do
    REGEX="${REGEX}${a}"
    [ "$arg" == $a ] && break
  done
  REGEX="([${REGEX}])"

  if [ -z $@ ]; then
    run_lsp
  else
    $TODO_SH command ls "$REGEX" | ag -Q "$@"
  fi
}
