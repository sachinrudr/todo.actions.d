import childProcess from 'node:child_process';
import * as fs from 'fs';
import React from 'react';
import stripAnsi from 'strip-ansi';
import {Text, Box, useFocus} from 'ink';
import {Item} from 'jstodotxt';

export interface TodoItemProps {
  item: Item
}

function TodoItem({item}: TodoItemProps) {
  const {isFocused} = useFocus();

  return (
    <Text inverse={isFocused}>{item.toString()}</Text>
  );
}

function comparePriority(a: Item, b: Item) {
  return (a?.priority() ?? 'Z').localeCompare(
    b?.priority() ?? 'Z')
}

export default function TodoView() {
  const [items, setItems] = React.useState<Item[]>([]);

  React.useEffect(() => {
    let env: {[key: string]: any} = {};

    // get env vars from config file
    const subProcess = childProcess.spawn('sh', ['-c',
      'source ~/.config/todo/config && env | grep TODO',
    ]);
    subProcess.stdout.on('data', (newOutput: Buffer) => {
      const lines = stripAnsi(newOutput.toString('utf8')).split('\n');
      lines.forEach(line => {
        const sp: string[] = line.split('=');
        env[sp[0] || ''] = sp[1];
      });

      const todoDir = env['TODO_DIR'];
      fs.readFile(`${todoDir}/todo.txt`, 'utf8', (err, data) => {
        if (err) throw err;

        const items = data.split('\n').map(s => new Item(s));
        setItems(items.sort(comparePriority));
      });
    });
  }, [setItems]);

  return (
    <Box flexDirection="column" padding={1}>
      {items.map((item, i) => 
        <TodoItem key={i} item={item}/>
      )}
    </Box>
  );
}
