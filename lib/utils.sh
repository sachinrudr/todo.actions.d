#!/bin/sh

if [[ "$OSTYPE" == "darwin"* ]]; then
  DATECMD=gdate
else
  DATECMD=date
fi

expand_filename() {
  FN="$1"

  [[ ! -f $FN ]] && FN="$FN.txt"
  [[ ! -f $FN ]] && FN="${FN%.txt}.todo.txt"

  printf "$FN"
}
