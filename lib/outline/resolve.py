#!/usr/bin/env python3
import sqlite3
from string import ascii_uppercase
import os
import logging as l
import schema as s

TODOTXT_DB = os.path.join(os.path.dirname(__file__), 'todo.db')


def get_task(db, id):
  # TODO we can do this in a single query
  c = db.cursor()
  c.execute('''
    SELECT text, raw, priority, completed FROM task WHERE id = ?
    ''', (id,))
  res = c.fetchone()
  if not res:
    return None
  text, raw, priority, completed = res

  c.execute('''
    SELECT p.name FROM project p
    JOIN task_projects tp ON tp.project_id = p.id
    WHERE tp.task_id = ?
    ''', (id,))
  res = c.fetchall()
  projects = [s.Project(name) for row in res for name in row]

  c.execute('''
    SELECT c.name FROM context c
    JOIN task_contexts tc ON tc.context_id = c.id
    WHERE tc.task_id = ?
    ''', (id,))
  res = c.fetchall()
  contexts = [s.Context(name) for row in res for name in row]

  c.execute('''
    SELECT key, value FROM tag WHERE task_id = ?
    ''', (id,))
  res = c.fetchall()
  tags = map(lambda row: s.Tag(key=row[0], value=row[1]), res)

  # c.execute('''
  #   SELECT parent_task_id FROM dependencies WHERE child_task_id = ?
  #   ''', (id,))
  
  return s.Task(
      raw=raw,
      text=text,
      priority=None if priority == None else s.Priority.get(priority),
      completed=completed,
      projects=projects,
      contexts=contexts,
      tags=tags,
      depends=[])

def sync_task(db, from_outline,
    text, raw, projects, contexts, tags, priority, completed, creation_date, completion_date,
    parent_task_id):
  """
  params:
    args necessary for task
  returns:
    (ok, task_id)
  """ 
  task_id, task = (None, None)
  created, updated = (False, False)
  project_ids, context_ids = ([], [])
  # convert input to database form
  priority_code = s.Priority[priority].value if priority else None
  
  c = db.cursor()
  res = c.execute('SELECT id, text FROM task WHERE raw = ?', (raw, )).fetchone()
  if res:
    task_id, _ = res
    task = get_task(db, task_id) 

  if task == None:
    c.execute('INSERT INTO task (text, raw, priority) VALUES (?,?,?)',
        (text, raw, priority_code))
    task_id = c.lastrowid
    created = True
  elif not completed and (task.priority.value if task.priority else None) != priority_code:
    c.execute('UPDATE task SET priority = ? WHERE id = ?',
        (priority_code, task_id))
    updated = True
  elif task.completed != completed:
    c.execute('UPDATE task SET completed = ?, completion_date = ? WHERE id = ?',
        (completed, completion_date, task_id))
    updated = True

  for project in projects:
    res = c.execute('SELECT id FROM project WHERE name = ?', (project,)).fetchone()
    if res:
      project_ids.append(res[0])
    else:
      c.execute('INSERT INTO project (name) VALUES (?)',
          (project,))
      project_ids.append(c.lastrowid)

  for context in contexts:
    res = c.execute('SELECT id FROM context WHERE name = ?', (context,)).fetchone()
    if res:
      context_ids.append(res[0])
    else:
      c.execute('INSERT INTO context (name) VALUES (?)',
          (context,))
      context_ids.append(c.lastrowid)
  
  c.executemany('INSERT INTO task_projects (task_id, project_id) VALUES (?,?)',
      [(task_id, project_id) for project_id in project_ids])

  c.executemany('INSERT INTO task_contexts (task_id, context_id) VALUES (?,?)',
      [(task_id, context_id) for context_id in context_ids])

  c.executemany('INSERT INTO tag (task_id, key, value) VALUES (?,?,?)',
      [(task_id, key, value) for key, value in tags])
  
  if task:
    stale_projects = [p.name for p in task.projects if p.name not in projects]
    stale_contexts = [c.name for c in task.contexts if c.name not in contexts]
    stale_tags = [(t.key, t.value) for t in task.tags if (t.key, t.value) not in tags]
    
    if any((stale_projects, stale_contexts, stale_tags)):
      c.executemany('''
        DELETE FROM task_projects 
        WHERE id IN (
          SELECT tp.id FROM task_projects tp 
          JOIN project p ON p.id = tp.project_id 
          WHERE p.name = ? AND tp.task_id = ?
          )
        ''', [(p, task_id) for p in stale_projects])
      c.executemany('''
        DELETE FROM task_contexts 
        WHERE id IN (
          SELECT tc.id FROM task_contexts tc 
          JOIN context c ON c.id = tc.context_id
          WHERE c.name = ? AND tc.task_id = ?
          )
        ''', [(c, task_id) for c in stale_contexts])
      c.executemany('DELETE FROM tag WHERE task_id = ? AND key = ? AND value = ?',
          [(task_id, k, v) for k,v in stale_tags])
      updated = True

  if parent_task_id:
    c.execute('INSERT INTO dependencies (parent_task_id, child_task_id) VALUES (?,?)',
        (parent_task_id, task_id))

  if created:
    l.debug('New Task: {} {}'.format(task_id, text))
  elif updated:
    l.debug('Updated Task: {} {}'.format(task_id, text))
    # if not from_outline:
    #   desync_task(db, task_id)

  db.commit()
  return (created or updated), task_id


def get_db():
  db_exists = os.path.exists(TODOTXT_DB)
  conn = sqlite3.connect(TODOTXT_DB)
  c = conn.cursor()
  c.execute("PRAGMA foreign_keys = ON")
  if not db_exists: setup_db(conn) 
  return conn


def setup_db(db):
  with open(os.path.join(os.path.dirname(__file__), 'schema-up.sql')) as f:
    db.cursor().executescript(f.read())


def reset_db():
  if os.path.exists(TODOTXT_DB):
    os.remove(TODOTXT_DB)
  setup_db(get_db())
