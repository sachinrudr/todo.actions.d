#!/usr/bin/env python3
import graphene as g
from string import ascii_uppercase
import logging as l
import resolve


Priority = g.Enum('Priority', zip(ascii_uppercase, range(26)))


class Project(g.ObjectType):
  name = g.String()


class Context(g.ObjectType):
  name = g.String()


class Tag(g.ObjectType):
  key = g.String()
  value = g.String()


class Task(g.ObjectType):
  raw = g.String(required=True)
  text = g.String(required=True)
  priority = g.Field(Priority)
  completed = g.Boolean(default_value=False)
  projects = g.List(Project)
  contexts = g.List(Context)
  tags = g.List(Tag)
  depends = g.List(lambda: Task)


# --- Query

class Query(g.ObjectType):
  task = g.Field(Task,
      id=g.String(),
      )

  def resolve_task(self, info, id):
    return resolve.get_task(info.context.get('db'), id)

schema = g.Schema(query=Query)
"""
# FOR FUTURE REFERENCE
# --- Mutations


class TagInput(g.InputObjectType):
  key = g.String()
  value = g.String()

class CreateTask(g.Mutation):
  class Arguments():
    raw = g.String(required=True)
    text = g.String(required=True)
    priority = g.String(required=False)
    projects = g.List(g.String)
    contexts = g.List(g.String)
    tags = g.List(TagInput)
    parent = g.Int(required=False)

  ok = g.Boolean()
  task_id = g.Int()
  
  @staticmethod
  def mutate(self, info, text, raw, projects, contexts, tags, priority=None, parent=None):
    db = info.context.get('db')
    ok, task_id = resolve.create_task(db, text, raw, projects, contexts, tags, priority, False, parent)
    return CreateTask(ok=ok, task_id=task_id)


class Mutation(g.ObjectType):
  create_task = CreateTask.Field()


# n den u gotta do sum like dis

      res = schema.execute('''
        mutation newTask(
            $raw: String!,
            $text: String!,
            $priority: String,
            $projects: [String]!,
            $contexts: [String]!,
            $tags: [TagInput]!,
            $parent: Int
            ) {
          createTask (
            raw: $raw,
            text: $text,
            priority: $priority
            contexts: $contexts,
            projects: $projects,
            tags: $tags,
            parent: $parent,
          )
          {
            ok
            taskId
          }
        }
        ''',
        variable_values={
          'raw': raw,
          'text': text,
          'priority': priority,
          'contexts': contexts,
          'projects': projects,
          'tags': [{'key': t[0], 'value': t[1]} for t in tags],
          'parent': parent_id,
        },
        context_value={'db': db})
"""
