-- Up

PRAGMA foreign_keys=on;

CREATE TABLE task (
  id INTEGER PRIMARY KEY,
  text TEXT NOT NULL,
  raw TEXT,
  priority INT,
  completed BOOL DEFAULT 0,
  creation_date INT DEFAULT CURRENT_TIMESTAMP,
  completion_date INT
);

CREATE TABLE project (
  id INTEGER PRIMARY KEY,
  name TEXT NOT NULL,
  UNIQUE(name) ON CONFLICT IGNORE
);

CREATE TABLE context (
  id INTEGER PRIMARY KEY,
  name TEXT NOT NULL,
  UNIQUE(name) ON CONFLICT IGNORE
);

CREATE TABLE tag (
  id INTEGER PRIMARY KEY,
  task_id INTEGER NOT NULL,
  key TEXT NOT NULL,
  value TEXT NOT NULL,
  FOREIGN KEY(task_id) REFERENCES task(id) ON DELETE CASCADE,
  UNIQUE(task_id, key) ON CONFLICT REPLACE
);

CREATE TABLE task_projects (
  id INTEGER PRIMARY KEY,
  task_id INTEGER NOT NULL,
  project_id INTEGER NOT NULL,
  FOREIGN KEY(task_id) REFERENCES task(id) ON DELETE CASCADE,
  FOREIGN KEY(project_id) REFERENCES project(id) ON DELETE CASCADE,
  UNIQUE(task_id, project_id) ON CONFLICT IGNORE
);

CREATE TABLE task_contexts (
  id INTEGER PRIMARY KEY,
  task_id INTEGER NOT NULL,
  context_id INTEGER NOT NULL,
  FOREIGN KEY(task_id) REFERENCES task(id) ON DELETE CASCADE,
  FOREIGN KEY(context_id) REFERENCES context(id) ON DELETE CASCADE,
  UNIQUE(task_id, context_id) ON CONFLICT IGNORE
);

CREATE TABLE dependencies (
  id INTEGER PRIMARY KEY,
  parent_task_id INTEGER NOT NULL,
  child_task_id INTEGER NOT NULL,
  FOREIGN KEY(parent_task_id) REFERENCES task(id) ON DELETE CASCADE,
  FOREIGN KEY(child_task_id) REFERENCES task(id) ON DELETE CASCADE
);
