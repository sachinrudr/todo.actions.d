#!/usr/bin/env python3
import argparse
import re
import os
import sys
from os import listdir, environ as env
from os.path import join
from resolve import get_db, sync_task
from schema import schema
from datetime import datetime
strftime = datetime.strftime
import logging as l


IGNORE_FILES = [
    'done.txt',
    'report.txt',
    'links.todo.txt',
    ]
NOTES_SEP = '---'
TODO_DIR = '/'.join(env['TODO_FILE'].split('/')[:-1])


basename = lambda fn: fn.split('/')[-1]


def find_outline_file(fn):
  """tries to find full filename of outline file given short name
  ex. study => $TODO_DIR/study.ol.txt
  """
  orig_fn = fn
  if not fn:
    l.error('File expected, but no file given')
    sys.exit(1)

  if not os.path.exists(fn):
    fn = re.sub('todo.txt$', 'ol.txt', fn)
    if not os.path.exists(fn):
      fn = join(TODO_DIR, fn)
      if not os.path.exists(fn):
        l.error('No file found matching name: {}'.format(orig_fn))
        sys.exit(1)
  return fn


def parse_tree(lines):
  """
  Parse an indented outline into (level, text, parent) tuples.  Each level
  of indentation is 4 spaces.
  """
  regex = re.compile(r'^(?P<indent>(?: {2})*)(?P<text>.*)')
  stack = []
  for index, line in enumerate(lines):
    if line.strip() == NOTES_SEP:
      break

    if not line.strip():
        continue

    match = regex.match(line)
    if not match:
      raise ValueError('Indentation not a multiple of 2 spaces: "{0}"'.format(line))

    level = len(match.group('indent')) // 2
    if level > len(stack):
      raise ValueError('Indentation too deep: "{0}"'.format(line))

    stack[level:] = [match.group('text')]
    parent = None
    if level:
      parent = stack[level - 1]
    yield level, match.group('text'), parent, index


# TODO: handle the case when you edit the parent, how do you reparse each task

def process_line(line):
  """handle one line in outline file
  returns
    (projects, contexts, tags, proority (bool)
    rest are lists)
  """
  tokens = line.split()
  projects = filter(lambda s: s.startswith('+'), tokens)
  contexts = filter(lambda s: s.startswith('@'), tokens)
  tags = map(lambda s: tuple(s.split(':')),
      filter(lambda s: bool(re.match('\w+:[\w]+', s)), 
        tokens))
  ps = map(lambda s: re.match('\([A-Z]\)', s), tokens)
  priority = next((m.group()[1] for m in ps if m), None)
  completed = tokens and tokens[0] == 'x'
  dates = (t for t in tokens if re.match('^\d{4}-\d{2}-\d{2}$', t))
  creation_date = next(dates, None)
  completion_date = next(dates, None)

  projects, contexts, tags = tuple(map(list, (projects, contexts, tags)))
  # get raw text without extra
  # TODO: optimize this when the time comes
  all_special = (projects + contexts + 
      [':'.join(p) for p in tags] +
      (['({})'.format(priority)] if priority else []) + 
      (['x'] if completed else []) +
      ([creation_date] if creation_date else []) +
      ([completion_date] if completion_date else [])
      )
  raw = ' '.join([t for t in tokens if t not in all_special])
  creation_date, completion_date = tuple(map(lambda t: datetime.strptime(t, '%Y-%m-%d') if t else None, 
    (creation_date, completion_date)))
  
  projects, contexts, tags = tuple(map(set, (projects, contexts, tags)))
  return (projects, contexts, tags, priority, raw, completed, creation_date, completion_date)


def expand(nodes):
  """take nodes and parse them
  returns
    dict where keys are text, and values are tuples
  """
  nodesd = {}
  for level, text, parent, line_num in nodes:
    projects, contexts, tags, priority, raw, completed, creation_date, completion_date = process_line(text)
    original_parent = parent
    
    # fold in any contexts, projects, or tags from parents
    while parent:
      _, _, parent, pprojects, pcontexts, ptags, ppriority, praw, _, _, _ = nodesd[parent]
      projects = projects | pprojects
      contexts = contexts | pcontexts
      tags = tags | ptags
      # adopt the first priority you see up the tree
      if priority is None:
        priority = ppriority
      # from parent, take only the first part of the raw
      raw = raw + '; ' + praw.split(';')[0] if praw else raw

    nodesd[text] = (level, text, original_parent, projects, contexts, tags, priority, raw,
        completed, creation_date, completion_date)
  return nodesd


def save(nodes, nodesd, from_outline):
  """sync todo tasks with db"""
  db = get_db()

  # map of text to ids
  id_map = {}
  for level, text, parent, line_num in nodes:
    # notes section starts
    if text == NOTES_SEP:
      break

    # hash node and if hash not in db add this to db
    # add appropriate ones to TODO.txt if they are not there
    # if it's not there in todo.txt OR done.txt, then mark that task as done in the outline and add the next task
    # raw basically counts as a hash
    _, _, _, projects, contexts, tags, priority, raw, completed, creation_date, completion_date = nodesd[text]
    parent_id = None

    if completed:
      continue

    # get the first parent task that you find walking up the tree
    while parent:
      _, ptext, parent, _, _, _, _, praw, _, _, _ = nodesd[parent]
      if praw and parent_id is None:
        parent_id = id_map[ptext]

    if raw:
      _, task_id = sync_task(db, from_outline, 
          text, raw, projects, contexts, tags, priority, False, creation_date, completion_date, parent_id)
      id_map[text] = task_id 
  return id_map


def distill(nodes, nodesd):
  """pull out the next action items from an outline"""
  # map of top level tasks -> most relevant leaf task
  distilled = {}
  for level, text, parent, line_num in nodes:
    _, _, _, projects, contexts, tags, priority, raw, completed, creation_date, completion_date = nodesd[text]
    
    root = None
    while parent:
      _, _, new_parent, _, _, _, _, praw, _, _, _ = nodesd[parent]
      if praw:
        root = parent
      parent = new_parent
    
    if raw:
      source = root if root else text
      if source in distilled:
        _, prev_level = distilled[source]
        if level > prev_level:
          distilled[source] = (text, level)
      else:
        distilled[source] = (text, level)
  
  return [text for source, (text, level) in distilled.items()]


def sync_distilled(new_fn, distilled, nodes, nodesd):
  """save distilled to *.todo.txt file, or update db with changes to created file"""
  lines = []
  for t in distilled:
    _, _, _, projects, contexts, tags, priority, raw, completed, creation_date, completion_date = nodesd[t]
    projects, contexts, tags = tuple(map(sorted, (projects, contexts, tags)))
    lines.append(''.join([
        '({}) '.format(priority) if priority and not completed else '',
        'x ' if completed else '',
        strftime(creation_date, '%Y-%m-%d') if creation_date else '',
        ' ' if creation_date or completion_date else '',
        strftime(completion_date, '%Y-%m-%d ') if completion_date else '',
        ' '.join(contexts) + (' ' if contexts else ''),
        ' '.join(projects) + (' ' if projects else ''),
        raw,
        (' ' if tags else '') + ' '.join(':'.join(t) for t in tags),
        '\n'
        ]).replace('  ', ' '))
  # everwrite file if exists
  with open(new_fn, 'w') as f:
    for l in lines:
      f.write(l)
