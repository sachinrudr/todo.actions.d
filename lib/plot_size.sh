#!/bin/sh
# plot sze of task list over time
read -r -d '' plotscript <<PLOTSCRIPT
set datafile sep ' ';
set xdata time;
set timefmt '%Y-%m-%dTT%H:%M:%S';
set yrange [0:200];
set terminal dumb size `tput cols`,`tput lines`;
plot '~/drive/todo/report.txt' using 1:2 w steps
PLOTSCRIPT

gnuplot -e "$plotscript"
