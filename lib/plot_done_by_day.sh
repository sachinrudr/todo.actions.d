#!/bin/sh
# plot done tasks per day with gnuplot
mv "$filename" "${filename}.csv"
filename="$filename.csv"

cat ~/drive/todo/done.txt | \
  awk '{print $2}' | \
  sort | \
  uniq -c | \
  awk '{print $2","$1}' > $filename

read -r -d '' plotscript <<PLOTSCRIPT
set xdata time;
set timefmt "%Y-%m-%d";
set autoscale;
set datafile sep ",";
set terminal dumb size `tput cols`,`tput lines`;
plot "$filename" using 1:2 w lines;
PLOTSCRIPT

gnuplot -p -e "$plotscript"
rm $filename
