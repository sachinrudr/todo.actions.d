#!/usr/bin/env python3
# import tasks from jira HTML page

import os
import sys
import logging
from os import environ as env
from bs4 import BeautifulSoup


todo_filename = 'securiti.todo.txt'
normalize_str = lambda s, sep: s.lower().replace(' ', sep)


def usage():
    print('usage: {} import tasks from jira'.format(sys.argv[0]))
    sys.exit(1)

def get_text(s):
    if not s:
        return None
    return s.text.strip().split('\n')[0]


priority_map = {
        'Highest': 'A',
        'High': 'B',
        'Medium': 'C',
        'Low': 'D',
}


def get_tasks_from_issue_navigator(b):
    tb = b.find('tbody')
    issues = tb.find_all('tr', attrs={'class': 'issuerow'})
    tasks = []

    for iss in issues:
        try:
            key = iss['data-issuekey']

            text = get_text(iss.find('td', attrs={'class': 'summary'}))
            itype = iss.find('td', attrs={'class': 'issuetype'}).find('img')['alt'].lower()
            status = get_text(iss
                    .find('td', attrs={'class': 'status'})
                    .find('span', attrs={'class': 'jira-issue-status-lozenge'}))
            
            s = iss.find('td', attrs={'class': 'priority'})
            priority = s.find('img')['alt'] if s else None

            if key and text and status:
                tasks.append({
                    'priority': priority_map[priority] if priority in priority_map else None,
                    'status': status,
                    'key':  key,
                    'text': text,
                    'itype': itype,
                    })
        except:
            logging.warn('skipped {}: {}'.format(key, iss.text))
    return tasks


def sync_tasks(tasks):
    """get task into securiti.ol.txt"""
    global todo_filename
    filename = os.path.join(os.path.dirname(env['TODO_FILE']), todo_filename)

    line_tokens = None
    with open(filename) as f:
        line_tokens = [l.split(' ') for l in f.read().split('\n')]

    added = set()
    with open(filename, 'w') as f:
        for i, t in enumerate(tasks):
            l = '{}{} +{} +{} {}'.format(
                    '({}) '.format(t['priority']) if t['priority'] else '',
                    t['key'],
                    t['itype'],
                    normalize_str(t['status'], '-'),
                    t['text'],
                    )
            f.write('{}\n'.format(l))

if __name__ == '__main__':
    if len(sys.argv) < 2 or \
            (not os.path.isfile(sys.argv[1])) or \
            (not env.get('TODO_FILE', False)):
        usage()

    # parse html and beautify
    new_html = None
    with open(sys.argv[1]) as f:
        # sys.setrecursionlimit(100000)
        # new_html = BeautifulSoup(f.read(), features='html.parser').prettify()
        new_html = f.read()
    
    # manually close td tags
    new_html_lines = []
    opened = False 
    for l in new_html.split('\n'):
        if '<td class=issuekey' in l:
            if opened:
                new_html_lines.append('</td>')
                opened = False
            else:
                opened = True
        if '</td>' in l:
            opened = False
        new_html_lines.append(l)

    # reparse html
    new_html = '\n'.join(new_html_lines)
    with open('/tmp/new.html', 'w') as out:
        out.write(new_html)

    # final sequence, parse html
    b = BeautifulSoup(new_html, features='html.parser')
    # parse tasks from html
    tasks = get_tasks_from_issue_navigator(b)
    # sync tasks to todo file
    sync_tasks(tasks)
