#!/usr/bin/env python3
"""
filter task files according to access.yaml
"""
import sys
import re
import os
import yaml
from subprocess import check_output
from datetime import datetime


def main(args):
  access_file = f'{os.environ["HOME"]}/.todo/access.yaml'
  with open(access_file, 'r') as f:
    config = yaml.safe_load(f)

  host = os.uname()[1].split('.')[0]
  if 'filters' not in config or host not in config['filters']:
    print(''.join(sys.stdin), end='')
    return True

  host_config = config['filters'][host]
  exclude = host_config['exclude'] if 'exclude' in host_config else []
  include = host_config['include'] if 'include' in host_config else []

  if len(exclude):
    for filename in sys.stdin:
      if os.path.basename(filename) in exclude:
        continue
      elif os.path.basename(filename).split('.')[0] in exclude:
        continue

      print(filename.strip())

  elif len(include):
    todo_dir = os.path.dirname(os.environ['TODO_FILE'])
    for filename in include:
        path = f'{os.path.join(todo_dir, filename)}.txt'
        if not os.path.exists(path):
          path = f'{os.path.join(todo_dir, filename)}.todo.txt'

        print(path)

  else:
    print(''.join(sys.stdin), end='')

  return True

if __name__ == "__main__":
	status = not main(sys.argv)
	sys.exit(status)
