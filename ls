#!/bin/sh
action=$1
shift
usage() {
  curcmd=`basename $0`
  echo "  $curcmd"
  echo "    Extends default ls command. Removes dates from start of lines in"
  echo "    order to make tasks from phone and CLI look the same"
  exit
}
[ "$action" = "usage" ] && usage

$TODO_FULL_SH command ls $@ \
  | sed -E 's/([0-9]+ )[0-9]{4}-[0-9]{2}-[0-9]{2} /\1/g'

[[ -z "$@" ]] && exit
# if args are there, then see if turns up anything using lf
$TODO_FULL_SH lf $@ 2>&1 >/tmp/todolf.output.txt
[ $? -eq 0 ] && cat /tmp/todolf.output.txt && rm /tmp/todolf.output.txt
